#ifndef SIMPLETASK_H
#define SIMPLETASK_H

#include "AbstractTask.h"

#include <QObject>

class SimpleTask : public AbstractTask
{
public:
    SimpleTask(QObject *parent);

    // AbstractTask interface
public:
    const TaskType type()  {return SIMPLE_TASK;};
    const QDateTime nextDue()  {return QDateTime();}; ///< returns a Null-DateTime, as SimpleTasks don't need to be completed at a certain time
    const bool isDone()  {return !completion().isNull();};
    const qint64 timeRemaining() {return -1;};

};

#endif // SIMPLETASK_H
