#include "AbstractTask.h"

AbstractTask::AbstractTask(QObject *parent) :
    QObject(parent),
    title(""),
    lastCompletion(QDateTime()),
    associatedCategories({}),
    importance(0),
    urgency(0),
    annoyance(0)
{

}

const QString AbstractTask::timeRemainingString()
{
    QString returnString;
    qint64 secondsRemaining = timeRemaining();

    if (secondsRemaining > 0)
    {
        QString timeString;
        qint64 minutesRemaining = 0;
        qint64 hoursRemaining = 0;
        qint64 daysRemaining = 0;
        qint64 monthsRemaining = 0;
        qint64 yearsRemaining = 0;

        if (secondsRemaining >= 60)
        {
            minutesRemaining = secondsRemaining / 60;
            secondsRemaining = secondsRemaining % 60;

            if (minutesRemaining >= 60)
            {
                hoursRemaining = minutesRemaining / 60;
                minutesRemaining = minutesRemaining % 60;

                if (hoursRemaining >= 24)
                {
                    daysRemaining = hoursRemaining / 24;
                    hoursRemaining = hoursRemaining % 24;

                    if (daysRemaining >= 30)
                    {
                        monthsRemaining = daysRemaining / 30;
                        daysRemaining = daysRemaining % 30;
                        if (monthsRemaining >= 12)
                        {
                            yearsRemaining = monthsRemaining / 12;
                            monthsRemaining = monthsRemaining % 12;
                        }
                    }
                }
            }
        }

        if (secondsRemaining > 0)
        {
            if (timeRemaining() < 60)
            timeString.prepend(QString("%1 %2").arg(secondsRemaining).arg((secondsRemaining > 1)?"seconds":"second"));
        }

        if (minutesRemaining > 0)
        {
            timeString.prepend(QString("%1 %2, ").arg(minutesRemaining).arg((minutesRemaining > 1)?"minutes":"minute"));
        }

        if (hoursRemaining > 0)
        {
            timeString.prepend(QString("%1 %2, ").arg(hoursRemaining).arg((hoursRemaining > 1)?"hours":"hour"));
        }

        if (daysRemaining > 0)
        {
            timeString.prepend(QString("%1 %2, ").arg(daysRemaining).arg((daysRemaining > 1)?"days":"day"));
        }

        if (monthsRemaining > 0)
        {
            timeString.prepend(QString("%1 %2, ").arg(monthsRemaining).arg((monthsRemaining > 1)?"months":"month"));
        }

        if (yearsRemaining > 0)
        {
            timeString.prepend(QString("%1 %2, ").arg(yearsRemaining).arg((yearsRemaining > 1)?"years":"year"));
        }


        returnString = QString("Due in %1").arg(timeString);

    }
    else
    {
        returnString = "Due now!";
    }

    return returnString;
}

void AbstractTask::setUrgency(int value)
{
    urgency = value;
    emit taskChanged(this);
}

void AbstractTask::setAnnoyance(int value)
{
    annoyance = value;
    emit taskChanged(this);
}

void AbstractTask::setCompleted(const QDateTime &value)
{
    lastCompletion = value;
    emit taskChanged(this);
}

void AbstractTask::setCompleted()
{
    lastCompletion = QDateTime::currentDateTime();
    emit taskChanged(this);
}

void AbstractTask::setCategories(const QSet<TaskCategory *> &newCategories)
{
    associatedCategories.clear();
    for (TaskCategory* cat : newCategories)
    {
        associatedCategories.insert(cat);
    }
}

void AbstractTask::setTitle(const QString &value)
{
    title = value;
    emit taskChanged(this);
}
/*
void AbstractTask::setStart(const QDateTime &value)
{
    startDateTime = value;
    emit taskChanged();
}*/

void AbstractTask::setImportance(int value)
{
    importance = value;
    emit taskChanged(this);
}
