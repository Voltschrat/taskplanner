#include "ManagerMainWindow.h"
#include "ui_ManagerMainWindow.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QMediaPlayer>
#include <QMediaPlaylist>

#include "TaskListModel.h"
#include "TaskDialog.h"
#include "CategoryEditor.h"

#include "SimpleTask.h"
#include "DeadlineTask.h"
#include "CyclicTask.h"

ManagerMainWindow::ManagerMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ManagerMainWindow),
    sortButtons(new QButtonGroup(this)),
    config()
{
    QApplication::setWindowIcon(QIcon(":/iconTimeLeft"));

    ui->setupUi(this);

    ui->completeButton->setEnabled(false);
    ui->deleteButton->setEnabled(false);

    tickTimer = new QTimer(this);
    tickTimer->setInterval(UPDATE_INTERVAL);
    tickTimer->setSingleShot(false);


    loadConfig();

    container = new DataContainer(this);
    connect(container, &DataContainer::error, this, &ManagerMainWindow::catchError);
    connect(container, &DataContainer::taskDueNotification, this, &ManagerMainWindow::showDueTaskNotification);
    container->loadTasks(config.taskfilePath);

    model = new TaskListModel(container);
    connect(model, &TaskListModel::error, this, &ManagerMainWindow::catchError);
    ui->taskListView->setModel(model);

    sortButtons->addButton(ui->sortByDefaultButton, SORT_DEFAULT);
    sortButtons->addButton(ui->sortByDeadlineButton, SORT_BY_DEADLINE);
    sortButtons->addButton(ui->sortByUrgencyButton, SORT_BY_URGENCY);
    sortButtons->addButton(ui->sortByTimeLeftButton, SORT_BY_TIMELEFT);

    for (QAbstractButton* p_A : sortButtons->buttons())
    {
        QPushButton* p_B =  (QPushButton*)p_A;
        p_B->setCheckable(true);
        p_B->setStyleSheet("QPushButton:checked {\
                           background-color: rgb(224, 150, 75);\
                border-style: inset;\
    }");
}


connect(ui->taskListView, &QListView::doubleClicked, [=](const QModelIndex &index)
{
    currentSelection = ui->taskListView->currentIndex();
    AbstractTask* task = model->taskForIndex(index);
    callTaskEditor(task);
});

connect(sortButtons, QOverload<int>::of(&QButtonGroup::buttonClicked), [=](int buttonID)
{
    changeSortOrder((SortOrderType)buttonID);
});

connect(ui->deleteButton, &QPushButton::clicked, [=]()
{
    currentSelection = ui->taskListView->currentIndex();
    if (currentSelection.isValid())
    {
        deleteTask(model->taskForIndex(currentSelection));
    }
});

connect(ui->taskListView, &QListView::clicked, [=](const QModelIndex &index)
{
    currentSelection = ui->taskListView->currentIndex();
    ui->completeButton->setEnabled(index.isValid());
    ui->deleteButton->setEnabled(index.isValid());

});

connect(tickTimer, &QTimer::timeout, [=]()
{
    currentSelection = ui->taskListView->currentIndex();
    model->refresh();
    if (currentSelection.isValid())
    {
        ui->taskListView->setCurrentIndex(currentSelection);
    }
});

connect(ui->completeButton, &QPushButton::clicked, [=]
{
    currentSelection = ui->taskListView->currentIndex();
    if (currentSelection.isValid())
    {
        AbstractTask* t = model->taskForIndex(currentSelection);
        t->setCompleted();
        model->refresh();
    }
});

connect(container, &DataContainer::taskDataHasChanged, [=]
{
    model->refresh();
}
);

connect(ui->newTaskButton, &QPushButton::clicked, this, &ManagerMainWindow::callNewTaskEditor);
connect(ui->actionCreateTask, &QAction::triggered, this, &ManagerMainWindow::callNewTaskEditor);

connect(ui->editCategoriesButton, &QPushButton::clicked, this, &ManagerMainWindow::callCategoryEditor);
connect(ui->actionEditTaskCategories, &QAction::triggered, this, &ManagerMainWindow::callCategoryEditor);

tickTimer->start();

}

ManagerMainWindow::~ManagerMainWindow()
{
    container->saveTasks(config.taskfilePath);
    saveConfig();
    delete ui;
}

bool ManagerMainWindow::loadConfig()
{
    QFile configFile(localConfigFilePath);
    if (configFile.exists() && configFile.open(QFile::ReadOnly))
    {
        qDebug() << "Reading config from "   << QFileInfo(configFile).absoluteFilePath();

        QString confString;
        QMap<QString, QString> confMap;

        //fill config Map
        while (!configFile.atEnd())
        {
            QString confString = configFile.readLine();
            qDebug() << "Read config line: " << confString;

            int dividerPos = confString.indexOf("=");
            if (dividerPos > 0)
            {
                QString key = confString.left(dividerPos+1).trimmed();
                QString value = confString.right(confString.length()-dividerPos).trimmed();
                confMap.insert(key, value);
            }
            else
            {
                qDebug() << "INVALID config line!";
            }

        }
        configFile.close();

        //translate config map to config struct
        if (!confMap.isEmpty())
        {
            if (confMap.contains("UserName"))
            {
                config.username = confMap.value("UserName");
                qDebug() << "read userName: " << config.username;
            }
            if (confMap.contains("TaskFilePath"))
            {
                config.taskfilePath = confMap.value("TaskFilePath");
                qDebug() << "read taskFilePath: " << config.taskfilePath;
            }
            else
            {
                return false;
            }
            return true;
        }
        else
        {
            qDebug() << "could not read any valid values from config file :(";
            return false;
        }
    }
    else
    {
        qDebug() << "no config file found.";
        return false;
    }
    return false;
}

void ManagerMainWindow::saveConfig()
{
    QFile configFile(localConfigFilePath);
    configFile.open(QFile::WriteOnly);

    QStringList configList;
    configList.append(QString("UserName = %1").arg(config.username));
    configList.append(QString("TaskFilePath = %1").arg(config.taskfilePath));

    for (QString confString : configList)
    {
        configFile.write(confString
                         .append("\r\n")
                         .toLatin1());
    }

    configFile.close();
}

void ManagerMainWindow::deleteTask(AbstractTask *task)
{
    if (task != nullptr)
    {
        int confirmation = QMessageBox::question(
                    this,
                    tr("Confirm Delete"),
                    tr("Shall the task \"%1\" really be deleted?")
                    .arg(task->getTitle()),
                    QMessageBox::Yes,
                    QMessageBox::No);

        if(confirmation == QMessageBox::Yes)
        {
            container->removeTask(task);
        }
    }
}


void ManagerMainWindow::callTaskEditor(AbstractTask* task)
{
    //  TaskDialog dia = TaskDialog(t, container, this);

    TaskDialog dia(task, container, this);
    int result = dia.exec();

    qDebug() << "Dialogue has returned with a value of" << result;
    if (result != 0)
    {
        if (task != nullptr)
        {
            qDebug() << "Edit Dialog:  accepted changes";

            TaskType newType = dia.getType();
            bool typeSwitched = false;
            if (task->type() != newType)
            {
                typeSwitched = true;
                container->removeTask(task);
            }

            switch(newType)
            {
            case DEADLINE_TASK :
            {
                if (typeSwitched)
                {
                    task = new DeadlineTask(container);
                }
                DeadlineTask *dTask = dynamic_cast<DeadlineTask*>(task);
                dTask->setDeadline(dia.getTaskDeadline());
                break;
            }
            case CYCLIC_TASK:
            {
                if (typeSwitched)
                {
                    task = new CyclicTask(container);
                }
                CyclicTask* cTask = dynamic_cast<CyclicTask*>(task);
                Cycle cycle = dia.getTaskCycle();
                cTask->setCycle(dia.getTaskCycle());
                break;
            }
            case SIMPLE_TASK :
            default:
                if (typeSwitched)
                {
                    task = new SimpleTask(container);
                }
                break;
            }
            if (typeSwitched)
            {
                container->addTask(task);
            }

            task->setTitle(dia.getTaskTitle());
            task->setCategories(dia.getSelectedCategories());
            task->setImportance(dia.getTaskImportance());
            task->setUrgency(dia.getTaskUrgency());
            task->setAnnoyance(dia.getTaskAnnoyance());

            if(dia.hasLastCompletedChanged())
            {
                task->setCompleted(dia.getlastCompleted());
            }
        }
        container->saveTasks(config.taskfilePath);
    }
    else
    {
        qDebug() << "Edit Dialog:  changes rejected";
    }
}

void ManagerMainWindow::callNewTaskEditor()
{
    //  TaskDialog dia = TaskDialog(t, container, this);

    TaskDialog dia(nullptr, container, this);
    int result = dia.exec();

    qDebug() << "Dialogue has returned with a value of" << result;
    if (result != 0)
    {
        AbstractTask* task;
        qDebug() << "Edit Dialog:  accepted changes";
        TaskType newType = dia.getType();
        switch(newType)
        {
        case DEADLINE_TASK :
        {
            task = new DeadlineTask(container);
            DeadlineTask *dTask = dynamic_cast<DeadlineTask*>(task);
            dTask->setDeadline(dia.getTaskDeadline());
            break;
        }
        case CYCLIC_TASK:
        {
            task = new CyclicTask(container);
            CyclicTask* cTask = dynamic_cast<CyclicTask*>(task);
            Cycle cycle = dia.getTaskCycle();
            cTask->setCycle(dia.getTaskCycle());
            break;
        }
        case SIMPLE_TASK :
        default:
            task = new SimpleTask(container);
            break;
        }

        task->setTitle(dia.getTaskTitle());
        task->setCategories(dia.getSelectedCategories());
        task->setImportance(dia.getTaskImportance());
        task->setUrgency(dia.getTaskUrgency());
        task->setAnnoyance(dia.getTaskAnnoyance());

        if(dia.hasLastCompletedChanged())
        {
            task->setCompleted(dia.getlastCompleted());
        }
        container->addTask(task);
        container->saveTasks(config.taskfilePath);
    }
    else
    {
        qDebug() << "Edit Dialog:  changes rejected";
    }
}

void ManagerMainWindow::callCategoryEditor()
{
    CategoryEditor catEdit(container->getCategories());
    catEdit.exec();

    for (TaskCategory* cat : catEdit.getNewCategories())
    {
        container->addCategory(cat);
    }
    for (TaskCategory* cat : catEdit.getRemovedCategories())
    {
        container->removeCategory(cat);
    }


}

void ManagerMainWindow::changeSortOrder(SortOrderType order)
{
    qDebug() << "SortButtonClicked:" << order;
    SortOrderType currentOrder = model->currentSortOrder();
    if (order == currentOrder && order != SORT_DEFAULT)
    {
        //sort button clicked again - deactivate and switch to default as long as clicked button is not SORT_DEFAULT
        sortButtons->button(currentOrder)->setChecked(false);
        sortButtons->button(SORT_DEFAULT)->setChecked(true);
        model->setSortOrder(SORT_DEFAULT);
    }
    else
    {
        sortButtons->button(currentOrder)->setChecked(false);
        //sortButtons->button(currentOrder)->backgroundRole().
        sortButtons->button(order)->setChecked(true);
        model->setSortOrder(order);
    }
}

void ManagerMainWindow::catchError(ErrorLevel level, QString msg)
{
    switch (level) {

    case CRITICAL:
        QMessageBox::critical(this,
                              tr("TaskPlanner encountered a critical error"),
                              msg, QMessageBox::Ok);
    case FATAL:
        QMessageBox::critical(this,
                              tr("TaskPlanner encountered a fatal error."),
                              msg.append(tr("\n\nThis Application will terminate now.")),
                              QMessageBox::Ok);
        QApplication::exit(13863);
    case WARNING:
    default:
        QMessageBox::warning(this,
                             tr("Warning"),
                             msg,
                             QMessageBox::Ok);
        break;
    }
}

void ManagerMainWindow::showDueTaskNotification(QVector<QString> list)
{
    QMediaPlayer soundPlayer;

    QMediaPlaylist play;
    play.addMedia(QUrl("qrc:/sounds/snd_bell.mp3"));
    play.setPlaybackMode(QMediaPlaylist::Loop);

    soundPlayer.setAudioRole(QAudio::AlarmRole);
    soundPlayer.setPlaylist(&play);
    soundPlayer.setVolume(100);

    QString listString;
    QListIterator<QString> i(list.toList());
    while (i.hasNext())
    {
        listString.append(i.next());
        if (i.hasNext())
        {
            listString.append(",\n");
        }
    }

    soundPlayer.play();
    QMessageBox::information(this,
                             tr("The following task(s) are due now!"),
                             listString, QMessageBox::Ok);
}
