#include "CategoryEditor.h"
#include "ui_CategoryEditor.h"

CategoryEditor::CategoryEditor(QList<TaskCategory *> categories, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CategoryEditor)
{
    ui->setupUi(this);

    updateButtonStates(ui->categoryList->currentItem(), ui->catNameLineEdit->text());

    for (TaskCategory* cat : categories)
    {
        QListWidgetItem *catItem = new QListWidgetItem(cat->getTitle(),ui->categoryList);
        categoryMap.insert(catItem, cat);
    }

    connect(ui->categoryList, &QListWidget::clicked, [=]()
    {
        updateButtonStates(ui->categoryList->currentItem(), ui->catNameLineEdit->text());
    });

    connect(ui->catNameLineEdit, &QLineEdit::textChanged, [=]()
    {
        updateButtonStates(ui->categoryList->currentItem(), ui->catNameLineEdit->text());
    });

    connect(ui->addCatButton, &QPushButton::clicked, this, &CategoryEditor::createCat);
    connect(ui->renameCatButton, &QPushButton::clicked, this, &CategoryEditor::renameCat);
    connect(ui->removeCatButton, &QPushButton::clicked, this, &CategoryEditor::deleteCat);

}

CategoryEditor::~CategoryEditor()
{
    delete ui;
}

void CategoryEditor::updateButtonStates(QListWidgetItem* item, QString newNameContent)
{
    if (newNameContent.isEmpty())
    {
        ui->addCatButton->setEnabled(false);
        ui->renameCatButton->setEnabled(false);
    }
    else
    {
        ui->addCatButton->setEnabled(true);
        if (item != nullptr)
        {
            ui->renameCatButton->setEnabled(true);
        }
    }

    if (item != nullptr)
    {
        ui->removeCatButton->setEnabled(true);
    }
    else
    {
        ui->removeCatButton->setEnabled(false);
        ui->renameCatButton->setEnabled(false);
    }
}

void CategoryEditor::createCat()
{
    QString catName(ui->catNameLineEdit->text());
    if (!catName.isEmpty())
    {
        TaskCategory* nCat = new TaskCategory(catName);
        QListWidgetItem *catItem = new QListWidgetItem(catName,ui->categoryList);
        categoryMap.insert(catItem, nCat);
        newCats.append(nCat);
    }
}

void CategoryEditor::renameCat()
{
    QListWidgetItem* currentItem = ui->categoryList->currentItem();
    QString catName = ui->catNameLineEdit->text();

    TaskCategory* setCat = categoryMap.value(currentItem);
    if (setCat != nullptr && !catName.isEmpty())
    {
        setCat->setTitle(catName);
        currentItem->setText(catName);
    }
}

void CategoryEditor::deleteCat()
{
    QListWidgetItem* currentItem = ui->categoryList->currentItem();
    TaskCategory* deadCat = categoryMap.value(currentItem);
    if (currentItem!=nullptr && deadCat != nullptr)
    {
        killCats.append(deadCat);
        categoryMap.remove(currentItem);
        ui->categoryList->removeItemWidget(currentItem);
        delete currentItem;
    }
}
