#ifndef TASKLISTMODEL_H
#define TASKLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "DataContainer.h"

#include "Definitions.h"
#include "AbstractTask.h"
#include "TaskCategory.h"

class TaskListModel : public QAbstractListModel
{
   Q_OBJECT

public:
    TaskListModel(DataContainer* useData);


public:
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    AbstractTask* taskForIndex(const QModelIndex &index) const;
    SortOrderType currentSortOrder() const {return sortOrder;};
    void setSortOrder(SortOrderType sortBy);

signals:
    void error(ErrorLevel level, QString message);

public slots:
    void rebuildTaskOrder();
    void refresh();

private:
    QList<TaskCategory*> categoryList;
    QMap<int, AbstractTask*> taskOrder;
    DataContainer* container;
    SortOrderType sortOrder;
    static bool compareDeadline(const QDateTime &t1, const QDateTime &t2);
    static bool compareTaskPriority(const AbstractTask* &t1, const AbstractTask* &t2);
    static bool compareTimeRemaining(AbstractTask *&t1, AbstractTask *&t2);


};

#endif // TASKLISTMODEL_H
