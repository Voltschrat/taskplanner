#ifndef MANAGERMAINWINDOW_H
#define MANAGERMAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QButtonGroup>
#include <QTimer>

#include "Definitions.h"
#include "DataContainer.h"
#include "TaskCategory.h"
#include "TaskListModel.h"

namespace Ui {
class ManagerMainWindow;
}

class ManagerMainWindow : public QMainWindow
{
    Q_OBJECT
 //   Q_DECLARE_METATYPE(ErrorLevel)

public:
    explicit ManagerMainWindow(QWidget *parent = nullptr);
    ~ManagerMainWindow();

private:
    Ui::ManagerMainWindow *ui;
    QTimer *tickTimer;
    QButtonGroup *sortButtons;
    DataContainer *container;
    TaskListModel *model;
    QModelIndex currentSelection;


    Configuration config;

    bool loadConfig();
    void saveConfig();
    void deleteTask(AbstractTask* task);


public slots:
    void callTaskEditor(AbstractTask* task);
    void callNewTaskEditor();
    void callCategoryEditor();
    void changeSortOrder(SortOrderType order);
    void catchError(ErrorLevel level, QString msg);
    void showDueTaskNotification(QVector<QString> list);

};

#endif // MANAGERMAINWINDOW_H
