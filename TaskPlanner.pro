#-------------------------------------------------
#
# Project created by QtCreator 2019-08-05T14:57:38
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

release:TARGET = TaskPlanner
#TARGET = TaskPlanner_d
TEMPLATE = app

DESTDIR = bin


OBJECTS_DIR = tmp/obj
MOC_DIR = tmp/moc
RCC_DIR = tmp/rcc
UI_DIR = tmp/ui

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
!build_pass:message("TaskPlanner build dir: $$PWD")


SOURCES += \
    AbstractTask.cpp \
    CategoryEditor.cpp \
    CyclicTask.cpp \
    DataContainer.cpp \
    DeadlineTask.cpp \
    SimpleTask.cpp \
    TaskDialog.cpp \
    TaskListModel.cpp \
    main.cpp \
    ManagerMainWindow.cpp \
    TaskCategory.cpp

HEADERS += \
    AbstractTask.h \
    CategoryEditor.h \
    CyclicTask.h \
    DataContainer.h \
    DeadlineTask.h \
    Definitions.h \
    ManagerMainWindow.h \
    SimpleTask.h \
    TaskCategory.h \
    TaskDialog.h \
    TaskListModel.h

FORMS += \
    CategoryEditor.ui \
    ManagerMainWindow.ui \
    TaskDialog.ui

RESOURCES += plannerResources.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
