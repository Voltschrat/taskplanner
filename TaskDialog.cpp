#include "TaskDialog.h"
#include "ui_TaskDialog.h"

#include <QDebug>
#include <CyclicTask.h>
#include <DeadlineTask.h>

TaskDialog::TaskDialog(AbstractTask *task, DataContainer* container, QWidget *parent) :
    QDialog(parent),
    weekdayGroup(new QButtonGroup(this)),
    ui(new Ui::TaskDialog),
    editingTask(nullptr)
{
    ui->setupUi(this);

    //assign weekdayPickers to buttongroup
    weekdayGroup->addButton(ui->dayPickerMonday, MONDAY);
    weekdayGroup->addButton(ui->dayPickerTuesday, TUESDAY);
    weekdayGroup->addButton(ui->dayPickerWednesday, WEDNESDAY);
    weekdayGroup->addButton(ui->dayPickerThursday, THURSDAY);
    weekdayGroup->addButton(ui->dayPickerFriday, FRIDAY);
    weekdayGroup->addButton(ui->dayPickerSaturday, SATURDAY);
    weekdayGroup->addButton(ui->dayPickerSunday, SUNDAY);
    weekdayGroup->setExclusive(false);
    usedContainer = container;

    setWeekdayPickersActive(false);

    qDebug() << "TaskDialog:: setup basic UI";


    updateCategories();

    qDebug() << "Setting up connections";
    connect(ui->categorySelectionListWidget, &QListWidget::itemSelectionChanged, this, &TaskDialog::updateCategorySelections);
    connect(ui->timeModeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=] (int index)
    {
        qDebug() << "TaskType is now " << index;
        TaskType type = (TaskType)index;
        switchTaskType(type);
    });

    connect(ui->repetitionIntervaltypeSpinbox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=] (int index)
    {
        qDebug() << "taskRepetitionMode is now " << index;
        CycleType mode = (CycleType)index;
        switchRepetitionMode(mode);
    });

    connect(ui->lastCompletedClearButton, &QPushButton::clicked, [=]
    {
        ui->lastCompletedDisplay->setText("never");
        lastCompleted=QDateTime();
        lastCompletedChanged = true;
    });

    connect(ui->setCompletedButton, &QPushButton::clicked, [=]
    {
        lastCompleted=QDateTime::currentDateTime();
        ui->lastCompletedDisplay->setText(lastCompleted.toString());
        lastCompletedChanged = true;
    });


    qDebug() << "TaskDialog constructor finished";


    if (task != nullptr)
    {
        qDebug() << "reading settings from task....";
        readSettingsFromTask(task);
        updateCategories();
    }

}

TaskDialog::~TaskDialog()
{
    delete ui;
}

void TaskDialog::readSettingsFromTask(AbstractTask *task)
{
    if (task != nullptr)
    {
        editingTask = task;
        ui->taskNameLineEdit->setText(task->getTitle());
        ui->importanceSpinBox->setValue(task->getImportance());
        ui->urgencySpinBox->setValue(task->getUrgency());
        ui->annoyanceSpinBox->setValue(task->getAnnoyance());

        switchTaskType(task->type());

        if (task->type() == DEADLINE_TASK)
        {
            ui->deadlineEdit->setDateTime(task->nextDue());
        }
        else if (task->type() == CYCLIC_TASK)
        {
            CyclicTask* cT = dynamic_cast<CyclicTask*>(task);
            Cycle c = cT->getCycle();
            ui->repetitionIntervaltypeSpinbox->setCurrentIndex(c.type);
            ui->repetitionAmountSpinbox->setValue(c.periodLength);
            ui->repetitionStartDateTimePicker->setDateTime(c.startTime);
            if (c.type == WEEKLYDAYS)
            {
                setWeekdayPickersActive(true);
                for (WeekDayType day : c.weekDays)
                {
                    weekdayGroup->button(day)->setChecked(true);
                }
            }
            else
            {
                setWeekdayPickersActive(false);
            }

        }
        else
        {
            ui->deadlineEdit->setDate(QDate::currentDate().addDays(1));
            ui->repetitionStartDateTimePicker->setDate(QDate::currentDate());
        }
        lastCompleted = task->completion();
        if (!lastCompleted.isNull())
        {
            ui->lastCompletedDisplay->setText(lastCompleted.toString());
        }
    }

}

QString TaskDialog::getTaskTitle()
{
    return ui->taskNameLineEdit->text();
}

int TaskDialog::getTaskImportance()
{
    return ui->importanceSpinBox->value();
}

int TaskDialog::getTaskUrgency()
{
    return ui->urgencySpinBox->value();
}

int TaskDialog::getTaskAnnoyance()
{
    return ui->annoyanceSpinBox->value();
}

QDateTime TaskDialog::getTaskDeadline()
{
    if (ui->deadlineEdit->isEnabled())
    {
        qDebug() << "task has deadline in dialog, returning DateTime" << ui->deadlineEdit->dateTime().toString();
        return ui->deadlineEdit->dateTime();
    }
    else
    {
        qDebug() << "task has NO deadline in dialog, returning empty DateTime";
        return QDateTime();
    }
}

QDateTime TaskDialog::getlastCompleted()
{
    return lastCompleted;
}

QDateTime TaskDialog::getCycleStart()
{
    return ui->repetitionStartDateTimePicker->dateTime();
}

bool TaskDialog::hasLastCompletedChanged()
{
    return lastCompletedChanged;
}

Cycle TaskDialog::getTaskCycle()
{
    Cycle p;
    if (ui->timeModeComboBox->currentIndex() == CYCLIC_TASK)
    {
        p.type = (CycleType)(ui->repetitionIntervaltypeSpinbox->currentIndex());
        p.periodLength = ui->repetitionAmountSpinbox->value();
        p.startTime = ui->repetitionStartDateTimePicker->dateTime();

        if (p.type == WEEKLYDAYS)
        {
            qDebug() << "\t > 5";
            p.weekDays = getSelectedWeekdays();
        }
    }
    else
    {
        p.type = NO_PERIOD;
    }
    return p;
}

TaskType TaskDialog::getType()
{
    return (TaskType)ui->timeModeComboBox->currentIndex();
}

void TaskDialog::updateCategories()
{
    qDebug() << "setting up categories";
    for (TaskCategory* cat : usedContainer->getCategories())
    {
        QListWidgetItem* newItem = new QListWidgetItem(cat->getTitle(), ui->categorySelectionListWidget);
        categoryItemMap.insert(cat,newItem);
       // newItem->setData(CategoryRole, QVariant());
        ui->categorySelectionListWidget->addItem(newItem);
    }

    //update initial category selection
    qDebug() << "update initial category selection";
    if (editingTask != nullptr)
    {

        for (TaskCategory* cat : categoryItemMap.keys())
        {
            QListWidgetItem *item = categoryItemMap.value(cat);

            if (item != nullptr)
            {
                if (editingTask->hasCategory(cat))
                {
                    item->setSelected(true);
                    item->setData(Qt::DecorationRole, QVariant(QIcon(":/checkmarkTrue")));
                    selectedCategories.insert(cat);
                }
                else
                {
                    item->setSelected(false);
                    item->setData(Qt::DecorationRole, QVariant(QIcon()));
                }
            }
        }
    }
}


void TaskDialog::updateCategorySelections()
{
    qDebug() << "-- updating category selection ---" ;
    selectedCategories.clear();

    if (ui->categorySelectionListWidget->count() > 0)
    {

        for (int i = 0; i < ui->categorySelectionListWidget->count(); ++i)
        {
            QListWidgetItem* selectionItem = ui->categorySelectionListWidget->item(i);
            TaskCategory* selectionCat = categoryItemMap.key(selectionItem);

            if (selectionItem->isSelected())
            {
                selectedCategories.insert(selectionCat);
                selectionItem->setData(Qt::DecorationRole, QVariant(QIcon(":/checkmarkTrue")));
                qDebug() << "updated added category" << selectionCat->getTitle();
            }
            else
            {
                selectionItem->setData(Qt::DecorationRole, QVariant(QIcon()));
            }
        }
    }
}

void TaskDialog::switchTaskType(TaskType type)
{
    switch (type) {
    case DEADLINE_TASK:
        ui->timeModeComboBox->setCurrentIndex(DEADLINE_TASK);
        ui->deadlineEdit->setEnabled(true);
        ui->repetitionStartDateTimePicker->setEnabled(false);
        ui->repetitionAmountSpinbox->setEnabled(false);
        ui->repetitionIntervaltypeSpinbox->setEnabled(false);
        setWeekdayPickersActive(false);
        ui->deadlineEdit->setDate(QDate::currentDate().addDays(1));
        break;
    case CYCLIC_TASK:
        ui->timeModeComboBox->setCurrentIndex(CYCLIC_TASK);
        ui->deadlineEdit->setEnabled(false);
        ui->repetitionStartDateTimePicker->setEnabled(true);
        ui->repetitionAmountSpinbox->setEnabled(true);
        ui->repetitionIntervaltypeSpinbox->setEnabled(true);
        if(ui->repetitionIntervaltypeSpinbox->currentIndex() == WEEKLYDAYS)
        {
            setWeekdayPickersActive(true);
        }
        else
        {
            setWeekdayPickersActive(false);
        }
        ui->repetitionStartDateTimePicker->setDate(QDate::currentDate());
        break;
    case SIMPLE_TASK:
    default:
        ui->timeModeComboBox->setCurrentIndex(SIMPLE_TASK);
        ui->deadlineEdit->setEnabled(false);
        ui->repetitionStartDateTimePicker->setEnabled(false);
        ui->repetitionAmountSpinbox->setEnabled(false);
        ui->repetitionIntervaltypeSpinbox->setEnabled(false);
        setWeekdayPickersActive(false);
        break;
    }
}

void TaskDialog::switchRepetitionMode(CycleType mode)
{
    setWeekdayPickersActive(mode==WEEKLYDAYS);
}

void TaskDialog::setWeekdayPickersActive(bool enable)
{
    for (QAbstractButton* p : weekdayGroup->buttons())
    {
        p->setEnabled(enable);
    }
}

QSet<WeekDayType> TaskDialog::getSelectedWeekdays()
{
    QSet<WeekDayType> days;
    if (weekdayGroup->button(MONDAY)->isChecked())
        days.insert(MONDAY);
    if (weekdayGroup->button(TUESDAY)->isChecked())
        days.insert(TUESDAY);
    if (weekdayGroup->button(WEDNESDAY)->isChecked())
        days.insert(WEDNESDAY);
    if (weekdayGroup->button(THURSDAY)->isChecked())
        days.insert(THURSDAY);
    if (weekdayGroup->button(FRIDAY)->isChecked())
        days.insert(FRIDAY);
    if (weekdayGroup->button(SATURDAY)->isChecked())
        days.insert(SATURDAY);
    if (weekdayGroup->button(SUNDAY)->isChecked())
        days.insert(SUNDAY);

    qDebug() << "Compiled weekdays. containing: " << days.size() << "entries.";
    qDebug() << "\t" << days.toList();

    return days;
}
