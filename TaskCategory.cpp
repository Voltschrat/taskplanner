#include "TaskCategory.h"

#include <QDebug>

TaskCategory::TaskCategory(QString title)
{
    this->title = title;
}

TaskCategory::~TaskCategory()
{

}

QString TaskCategory::getTitle() const
{
    return title;
}

void TaskCategory::setTitle(const QString &value)
{
    title = value;
}

const QList<AbstractTask *> TaskCategory::getAssociatedTasks() const
{
    return associatedTasks;
}

bool TaskCategory::contains(AbstractTask *task) const
{
    return associatedTasks.contains(task);
}
