#ifndef CATEGORYEDITOR_H
#define CATEGORYEDITOR_H

#include <QDialog>
#include <QListWidgetItem>
#include "TaskCategory.h"

namespace Ui {
class CategoryEditor;
}

class CategoryEditor : public QDialog
{
    Q_OBJECT

public:
    explicit CategoryEditor(QList<TaskCategory*> categories, QWidget *parent = nullptr);
    ~CategoryEditor();
    QList<TaskCategory*> getNewCategories() const {return newCats;};
    QList<TaskCategory*> getRemovedCategories() const {return killCats;};

private:
    Ui::CategoryEditor *ui;
    void updateButtonStates(QListWidgetItem *item, QString newNameContent);
    QMap<QListWidgetItem*, TaskCategory*> categoryMap;
    QList<TaskCategory*> newCats;
    QList<TaskCategory*> killCats;


private slots:
    void createCat();
    void renameCat();
    void deleteCat();
};

#endif // CATEGORYEDITOR_H
