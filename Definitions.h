#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <QString>
#include <QSet>
#include <QDateTime>
#include <QPair>

const QString localConfigFilePath = "./config.conf";
//const int TaskIdRole = Qt::UserRole;
//const int CategoryRole = Qt::UserRole+1;
const int UPDATE_INTERVAL = 3000;

//typedef QPair<ushort /*id*/, QString /*name*/> CategoryType;


enum TaskType
{
    SIMPLE_TASK,
    DEADLINE_TASK,
    CYCLIC_TASK
};

enum SortOrderType
{
    SORT_DEFAULT = 0,
    SORT_BY_URGENCY,
    SORT_BY_DEADLINE,
    SORT_BY_TIMELEFT
};

//Defunct
/*
enum TaskTimeModeType
{
    NO_TIMEMODE = 0,
    DEADLINE,
    CYCLIC,
};*/

enum CycleType
{
    NO_PERIOD = -1,
    DAILY,
    WEEKLY,
    WEEKLYDAYS,
    MONTHLY,
    YEARLY,
};
enum WeekDayType
{
    MONDAY = 0,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
};

enum ErrorLevel
{
    WARNING,
    CRITICAL,
    FATAL
};
 Q_DECLARE_METATYPE(ErrorLevel);

struct Cycle
{
    CycleType type;
    int periodLength;
    QSet<WeekDayType> weekDays;
    QDateTime startTime;
};


struct Configuration
{
    QString username = "";
    QString taskfilePath = "data.json";
};

#endif // DEFINITIONS_H
