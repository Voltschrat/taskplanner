#ifndef TASKDIALOG_H
#define TASKDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QDateTime>
#include <QButtonGroup>

#include "Definitions.h"
#include "AbstractTask.h"
#include "DataContainer.h"
#include "TaskCategory.h"



namespace Ui {
class TaskDialog;
}

class TaskDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TaskDialog(AbstractTask* task,  DataContainer* container, QWidget *parent = nullptr);
    ~TaskDialog();
    void readSettingsFromTask(AbstractTask* task);

    QSet<TaskCategory*> getSelectedCategories() {return selectedCategories;};
    QString getTaskTitle();
    int getTaskImportance();
    int getTaskUrgency();
    int getTaskAnnoyance();
    QDateTime getTaskDeadline();
    QDateTime getlastCompleted();
    QDateTime getCycleStart();

    bool hasLastCompletedChanged();
    Cycle getTaskCycle();
    TaskType getType();

signals:
    void newCategoryRequest(QString catName);

public slots:
    void updateCategories();

private:
    bool lastCompletedChanged = false;
    Ui::TaskDialog *ui;
    AbstractTask* editingTask;
    DataContainer* usedContainer;
    QSet<TaskCategory*> selectedCategories;
    QMap<TaskCategory*, QListWidgetItem*> categoryItemMap;
    QButtonGroup* weekdayGroup;
    QDateTime lastCompleted;
    void updateCategorySelections();
    void switchTaskType(TaskType type);
    void switchRepetitionMode(CycleType mode);
    void setWeekdayPickersActive(bool enable = true);
    QSet<WeekDayType> getSelectedWeekdays();

};

#endif // TASKDIALOG_H
