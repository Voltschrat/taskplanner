#ifndef TASKCATEGORY_H
#define TASKCATEGORY_H

#include <QString>
#include <QList>

class AbstractTask;
class DataContainer;

class TaskCategory
{

    friend  DataContainer;

public:
    TaskCategory(QString title);
    ~TaskCategory();

    QString getTitle() const;
    void setTitle(const QString &value);

    const QList<AbstractTask*> getAssociatedTasks() const;
    bool contains(AbstractTask*) const;

protected:
    QList<AbstractTask*> associatedTasks;

private:
    QString title;

};

//Q_DECLARE_METATYPE(TaskCategory*)

#endif // TASKCATEGORY_H
