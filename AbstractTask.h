#ifndef ABSTRACTTASK_H
#define ABSTRACTTASK_H

#include <QObject>

#include "Definitions.h"
#include "TaskCategory.h"

class AbstractTask : public QObject
{
    Q_OBJECT

public:

    virtual TaskType const type() = 0;
    virtual QDateTime const nextDue() = 0;
    virtual bool const isDone() = 0;
    virtual qint64 const timeRemaining() = 0; ///<returns remaining time in seconds
    virtual QString const timeRemainingString();

    explicit AbstractTask(QObject *parent = nullptr);
    QString getTitle() const {return title;};
    QDateTime completion() const {return lastCompletion;};
   // QDateTime start() const {return startDateTime;};
    QSet<TaskCategory*> categories() const {return associatedCategories;};
    int getImportance() const {return importance;};
    int getUrgency() const{return urgency;};
    int getAnnoyance() const{return annoyance;};

    const bool hasCategory(TaskCategory* category) {return categories().contains(category);};

    void setImportance(int value);
    void setUrgency(int value);
    void setAnnoyance(int value);

    void setCompleted(const QDateTime &value);
    void setCompleted();;
    void setCategories(const QSet<TaskCategory*> &newCategories);

    void setTitle(const QString &value);

    void setStart(const QDateTime &value);

signals:
    void taskChanged(AbstractTask* task);

public slots:

private:
    QString title;
   // QDateTime startDateTime;
    QDateTime lastCompletion;
    QSet<TaskCategory*> associatedCategories;
    int importance;
    int urgency;
    int annoyance;

};

#endif // ABSTRACTTASK_H
