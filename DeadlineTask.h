#ifndef DEADLINETASK_H
#define DEADLINETASK_H

#include "AbstractTask.h"

#include <QObject>

class DeadlineTask : public AbstractTask
{
public:
    DeadlineTask(QObject* parent);
    QDateTime getDeadline() {return deadline;};
    // AbstractTask interface:
    const TaskType type()  {return DEADLINE_TASK;};
    const bool isDone()  {return !completion().isNull();};
    const QDateTime  nextDue() {return deadline;};
    const qint64 timeRemaining();

    void setDeadline(const QDateTime &value);

private:
    QDateTime deadline;




};

#endif // DEADLINETASK_H
