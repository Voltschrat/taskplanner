#include "DeadlineTask.h"
#include <QDebug>

DeadlineTask::DeadlineTask(QObject* parent)
    : AbstractTask(parent)
{

}

const qint64 DeadlineTask::timeRemaining()
{
    if(isDone())
    {
       return -1;
    }
    qint64 secondsRemaining =
            deadline.toSecsSinceEpoch()
            - QDateTime::currentDateTime().toSecsSinceEpoch();
    if (secondsRemaining < 0 )
    {
        return 0;
    }
    else
    {
     return secondsRemaining;
    }
}

void DeadlineTask::setDeadline(const QDateTime &value)
{
    deadline = value;
    qDebug() << "TaskDeadline was set to " << value.toString();
    emit taskChanged(this);
}
