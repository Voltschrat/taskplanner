#include "DataContainer.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "SimpleTask.h"
#include "DeadlineTask.h"
#include "CyclicTask.h"

DataContainer::DataContainer(QObject *parent) : QObject(parent)
{
    connect(&dueCheckTimer, &QTimer::timeout, this, &DataContainer::checkForTimeouts);
    dueCheckTimer.setInterval(1000);
    dueCheckTimer.start();
}

DataContainer::~DataContainer()
{
    int counter = 0;
    for (AbstractTask *t : taskList)
    {
        delete t;
        counter++;
    }
    qDebug() << "Deleted " << counter << "tasks";
    counter = 0;

    for (TaskCategory*c : categoryList)
    {
        delete c;
        counter++;
    }
    qDebug() << "Deleted " << counter << "categories";

}

void DataContainer::addTask(AbstractTask *task)
{
    taskList.append(task);
    connect(task,  &AbstractTask::taskChanged, this, &DataContainer::onTaskDataChange);
    emit dataHasChanged();
}

void DataContainer::removeTask(AbstractTask *task)
{
    if (task != nullptr)
    {
        task->disconnect();
        taskList.removeOne(task);

        emit dataHasChanged();
        task->disconnect();
        task->deleteLater();
    }
    else
    {
        qDebug() << "DataContainer::removeTask(); instructed to remove task that is a NULLpointer.";
        if (taskList.contains(task))
        {
            taskList.removeOne(task);
            qDebug() << "\t<!> WARNING: DataContainer did contain a nullpointer. removed.";
            emit dataHasChanged();
        }
    }
}

void DataContainer::addCategory(TaskCategory *category)
{
    categoryList.append(category);
    emit categoriesChanged();
    qDebug() << "Added category " << category->getTitle();
}

void DataContainer::removeCategory(TaskCategory *category)
{
    for (AbstractTask* t : taskList)
    {
        if (t->hasCategory(category))
        {
            QSet<TaskCategory*> taskCats = t->categories();
            taskCats.remove(category);

        }
    }
    categoryList.removeOne(category);
    emit dataHasChanged();
    emit categoriesChanged();
    qDebug() << "Removed category " << category->getTitle();
}





void DataContainer::taskToJson(AbstractTask *task, QJsonObject &taskObject)
{
    taskObject["title"] = task->getTitle();
    taskObject["type"] = task->type();

    taskObject["lastCompleted"] = task->completion().toString();

    taskObject["importance"] = task->getImportance();
    taskObject["urgency"] = task->getUrgency();
    taskObject["annoyance"] = task->getAnnoyance();

    switch (task->type())
    {
    case DEADLINE_TASK:
        taskObject["deadline"] = ((DeadlineTask*)task)->getDeadline().toString();
        break;
    case CYCLIC_TASK:
    {
        taskObject["cycleType"] = ((CyclicTask*)task)->getCycle().type;
        taskObject["cycleLength"] = ((CyclicTask*)task)->getCycle().periodLength;
        taskObject["cycleStart"] = ((CyclicTask*)task)->getCycle().startTime.toString();
        QList<WeekDayType> weekdayList = ((CyclicTask*)task)->getCycle().weekDays.toList();
        QString weekdayString = "";
        for(int i = 0; i < weekdayList.size(); ++i)
        {
            weekdayString.append(QString::number((int)(weekdayList[i])));
            if (i < (weekdayList.size() -1))
            {
                weekdayString.append(", ");
            }
        }
        taskObject["cycleDays"] = weekdayString;
    }
        break;
    case SIMPLE_TASK:
    default:
        break;
    }
}

AbstractTask *DataContainer::taskFromJson(QJsonObject &taskObject)
{
    bool hadErrors = false;
    int typeInt = taskObject["type"].toInt(-1);

    qDebug() << "Reading Task" << taskObject["title"].toString("INVALID NAME");

    if (typeInt == -1)
    {

        emit error(CRITICAL, tr(""));
        return nullptr;
    }

    TaskType type = (TaskType)typeInt;
    AbstractTask *newTask;

    switch (type)
    {
    case DEADLINE_TASK:
    {
        DeadlineTask *dTask = new DeadlineTask(this);
        newTask = dTask;

        QDateTime deadline = QDateTime::fromString(taskObject["deadline"].toString());
        if (deadline.isValid())
        {
            dTask->setDeadline(deadline);
        }
        else
        {
            hadErrors = true;
        }
    }
        break;
    case CYCLIC_TASK:
    {
        CyclicTask *cTask = new CyclicTask(this);
        newTask = cTask;

        int cycleTypeInt = taskObject["cycleType"].toInt(-1);
        int cycleLenght = taskObject["cycleLength"].toInt(-1);
        QString cycleDayString = taskObject["cycleDays"].toString();
        QString cycleStartString = taskObject["cycleStart"].toString();

        if (cycleTypeInt == -1 || cycleLenght == -1 || cycleStartString.isNull())
        {
            hadErrors = true;
            break;
        }
        else if (cycleTypeInt == WEEKLYDAYS && cycleDayString.isNull())
        {
            qDebug() << "Error: WeeklyDayTask without days";
            hadErrors = true;
            break;
        }

        Cycle cycle;
        cycle.type = (CycleType)cycleTypeInt;
        cycle.periodLength = cycleLenght;
        cycle.startTime = QDateTime::fromString(cycleStartString);

        if (cycle.type == WEEKLYDAYS)
        {
            QStringList dayStringList = cycleDayString.split(",", QString::SkipEmptyParts);
            QSet<WeekDayType> weekDaySet;
            for (QString dayString : dayStringList)
            {
                dayString = dayString.trimmed();
                bool ok;
                int dayInt = dayString.toInt(&ok);
                if (ok && dayInt >= 0 && dayInt <7)
                {
                    weekDaySet.insert((WeekDayType)dayInt);
                }
                else
                {
                    qDebug() << "Could not convert int" << dayInt << " to weekday";
                    hadErrors = true;
                    break;
                }
            }
            if (weekDaySet.isEmpty())
            {
                hadErrors = true;
            }
            else
            {
                cycle.weekDays = weekDaySet;

            }

        }
        cTask->setCycle(cycle);
        break;
    }
    case SIMPLE_TASK:
    default:
        newTask = new SimpleTask(this);
        break;
    }

    QString title = taskObject["title"].toString("");
    int importance = taskObject["importance"].toInt(-1);
    int urgency = taskObject["urgency"].toInt(-1);
    int annoyance = taskObject["annoyance"].toInt(-1);
    QString completionString = taskObject["lastCompleted"].toString("");

    if (importance == -1 || urgency == -1 || annoyance == -1)
    {
        hadErrors = true;
    }
    else
    {
        newTask->setTitle(title);
        newTask->setImportance(importance);
        newTask->setUrgency(urgency);
        newTask->setAnnoyance(annoyance);
        if (!completionString.isEmpty())
        {
            QDateTime completion = QDateTime::fromString(completionString);
            newTask->setCompleted(completion);
        }
    }


    if (hadErrors)
    {
        qDebug() << "Errors while reading Task" << taskObject["title"].toString("INVALID NAME");
        newTask->deleteLater();
        return nullptr;
    }
    return newTask;
}

void DataContainer::checkForTimeouts()
{
    QVector<QString> notifyTasks;
    for (AbstractTask* t : taskList)
    {
        if (t->isDone())
        {
            dueTasks.removeOne(t);
        }
        else
        {
            if(t->nextDue() <= QDateTime::currentDateTime() && !dueTasks.contains(t))
            {
                notifyTasks.append(t->getTitle());
                dueTasks.append(t);
            }
        }
    }
    if (!notifyTasks.isEmpty())
    {
        emit taskDueNotification(notifyTasks);
    }
}

void DataContainer::onTaskDataChange(AbstractTask *task)
{
    qDebug() << "DataContainer::onTaskDataChange();";
    dueTasks.removeOne(task);
    emit taskDataHasChanged(task);
}

void DataContainer::loadTasks(const QString &taskfilePath)
{
    QFile taskFile(taskfilePath);

    qDebug() << "Opening task file " << taskFile.fileName();

    if(taskFile.open(QFile::ReadOnly))
    {
        QByteArray data = taskFile.readAll();
        QJsonObject dataObj = QJsonDocument::fromJson(data).object();

        //load Categories
        if (dataObj.contains("categories"))
        {
            QJsonArray categoryArray = dataObj["categories"].toArray();
            for (int i = 0; i < categoryArray.size(); ++i)
            {
                QJsonObject catObj = categoryArray[i].toObject();
                QString catTitle(catObj["title"].toString());
                if (!catTitle.isEmpty())
                {
                    TaskCategory* newCat = new TaskCategory(catTitle);
                    categoryList.append(newCat);
                }
            }
        }

        //load Tasks
        if (dataObj.contains("tasks"))
        {
            QJsonArray taskArray = dataObj["tasks"].toArray();
            for (int i = 0; i < taskArray.size(); ++i)
            {
                QJsonObject taskObj = taskArray[i].toObject();
                AbstractTask* newTask = taskFromJson(taskObj);
                if (newTask != nullptr)
                {
                    addTask(newTask);
                }
            }
        }


        taskFile.close();
    }
    else
    {
        emit error(CRITICAL, tr("Could not open data file \"%1\" for writing").arg(taskfilePath));
    }

}

void DataContainer::saveTasks(const QString &taskSavePath)
{
    QFile taskFile(taskSavePath);
    if (taskFile.open(QFile::WriteOnly))
    {

        QJsonObject saveObject;

        QJsonArray categoryArray;
        for (TaskCategory* cat : categoryList)
        {
            QJsonObject catObj;
            catObj["title"] = cat->getTitle();
            categoryArray.append(catObj);
        }

        QJsonArray taskArray;

        for (AbstractTask* task : taskList)
        {
            QJsonObject taskObj;
            taskToJson(task, taskObj);
            taskArray.append(taskObj);
        }

        saveObject["categories"] = categoryArray;
        saveObject["tasks"] = taskArray;

        QJsonDocument jsonDoc(saveObject);

        taskFile.write(jsonDoc.toJson());

        taskFile.close();
    }
    else
    {
        emit error(CRITICAL, tr("Could not open data file \"%1\" for reading").arg(taskSavePath));
    }
}

