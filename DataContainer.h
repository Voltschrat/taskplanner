#ifndef DATACONTAINER_H
#define DATACONTAINER_H

#include <QObject>
#include <QJsonObject>
#include <QTimer>

#include "AbstractTask.h"
#include "TaskCategory.h"


class DataContainer : public QObject
{
    Q_OBJECT

public:
    explicit DataContainer(QObject *parent = nullptr);
    ~DataContainer();
    int taskCount() const       {return taskList.size();};
    int categoryCount() const   {return categoryList.size();};
    QList<AbstractTask*> const getTasks() {return taskList;};
    QList<TaskCategory*> const getCategories() {return categoryList;};
    void addTask(AbstractTask* task);
    void removeTask(AbstractTask* task);
    void addCategory(TaskCategory* category);
    void removeCategory(TaskCategory* category);

public slots:
    void loadTasks(const QString &taskfilePath);
    void saveTasks(const QString &taskSavePath);
signals:
    void dataHasChanged();
    void taskDataHasChanged(AbstractTask* task);
    void categoriesChanged();
    void error(ErrorLevel level, QString message);
    void taskDueNotification(QVector<QString>);
private:
    QList<AbstractTask*> taskList;
    QVector<AbstractTask*> dueTasks;
    QList<TaskCategory*> categoryList;
    QTimer dueCheckTimer;

    void taskToJson(AbstractTask* task, QJsonObject &taskObject);
    AbstractTask* taskFromJson(QJsonObject &taskObject);
    void checkForTimeouts();
    void onTaskDataChange(AbstractTask* task);
};

#endif // DATACONTAINER_H
