#include "TaskListModel.h"
#include <QDebug>
//#include <QtAlgorithms>
#include <QIcon>

#include "SimpleTask.h"
#include "DeadlineTask.h"
#include "CyclicTask.h"

TaskListModel::TaskListModel(DataContainer *useData) :
    container(useData),
    sortOrder(SORT_DEFAULT)
{
    connect(container, &DataContainer::dataHasChanged, this, &TaskListModel::rebuildTaskOrder);


    rebuildTaskOrder();
}

void TaskListModel::rebuildTaskOrder()
{

    beginResetModel();
    taskOrder.clear();

    QList<AbstractTask*> rawTaskList = container->getTasks();

    if (sortOrder == SORT_BY_URGENCY)
    {
        QMultiMap<int, AbstractTask*> urgencyMap;
        for (int i = 0; i < rawTaskList.size(); ++i)
        {
            AbstractTask* currentTask = rawTaskList[i];
            //int currentTaskId = (container->taskIdList())[i];
            int currentUrgency = currentTask->getUrgency();
            urgencyMap.insertMulti(currentUrgency, currentTask);
        }

        int orderId = 0;
        for (int i = 5; i >=0; --i)
        {
            //for each urgency add the tasks to the list
            QList<AbstractTask*> urgencyTasks = urgencyMap.values(i); //tasks for urgency level i
            for (AbstractTask* thisTask : urgencyTasks)
            {
                taskOrder.insert(orderId, thisTask);
                orderId++;
            }
        }


    }
    else if (sortOrder == SORT_BY_DEADLINE)
    {
        QMultiMap<QDateTime, AbstractTask*> deadlineMap;
        for (int i = 0; i < rawTaskList.size(); ++i)
        {
            AbstractTask* currentTask = rawTaskList[i];

            QDateTime currentdeadLine;
            if (currentTask->type() == DEADLINE_TASK)
            {
                currentdeadLine =  (dynamic_cast<DeadlineTask*>(currentTask))->getDeadline();
            }


            deadlineMap.insertMulti(currentdeadLine, currentTask);
        }

        QList<QDateTime> timeList = deadlineMap.uniqueKeys();//list to store all occurring deadlines and sort them
        std::sort(timeList.begin(), timeList.end(), TaskListModel::compareDeadline);



        int orderId = 0;
        for (QDateTime deadline : timeList)
        {
            //for each deadline get all associated tasks and add them to the orderIdMap
            QList<AbstractTask*> associatedTasks = deadlineMap.values(deadline);
            for (AbstractTask* thisTask : associatedTasks)
            {
                taskOrder.insert(orderId, thisTask);
                orderId++;
            }
        }
    }
    else if (sortOrder == SORT_BY_TIMELEFT)
    {
        qDebug() << "<!> Sorting by TimeRemaining";
        QList<AbstractTask*> sortList;
        QList<AbstractTask*> remainingList;

        for (AbstractTask *t : container->getTasks())
        {
            if (!t->isDone())
            {
                sortList.append(t);
            }
            else
            {
                //task is done (at least for now). These will be appended to the end of the list after it is sorted
                remainingList.append(t);
            }
        }
        std::sort(sortList.begin(), sortList.end(), TaskListModel::compareTimeRemaining);
        std::sort(remainingList.begin(), remainingList.end(), TaskListModel::compareTimeRemaining);
        //remainingList is sorted too, so that CyclicTasks appear before Deadline- and SimpleTasks



        qDebug() << "List Sorted.";
        sortList.append(remainingList);
        int counter=0;
        for (AbstractTask* tP : sortList)
        {
            taskOrder.insert(counter, tP);
            counter++;
        }
    }
    else
    {
        //SORT BY DEFAULT
        for (int i = 0; i < rawTaskList.size(); ++i)
        {
            //Task* associatedTask =
            taskOrder.insert(i, rawTaskList[i]);
        }
    }
    endResetModel();
}

void TaskListModel::refresh()
{
    rebuildTaskOrder();
}

int TaskListModel::rowCount(const QModelIndex &parent) const
{
    return container->taskCount();
}

QVariant TaskListModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid())
    {
        AbstractTask* selectedTask = taskForIndex(index);

        if (selectedTask == nullptr)
        {
            return QVariant();
        }
        if (role == Qt::DecorationRole)
        {
            TaskType type = selectedTask->type();

            if ( type == SIMPLE_TASK && selectedTask->isDone())
            {
                return QVariant(QIcon(":/checkmarkTrue"));
            }
            else if (type == CYCLIC_TASK )
            {
                if (selectedTask->isDone())
                {
                    return QVariant(QIcon(":/cyclicDoneIcon"));
                }
                else if (selectedTask->timeRemaining() == 0)
                {
                    return QVariant(QIcon(":/cyclicDueIcon"));
                }
                else
                {
                    return QVariant(QIcon(":/cyclicTypeIcon"));
                }
            }
            else if (type == DEADLINE_TASK )
            {
                if (selectedTask->isDone())
                {
                    return QVariant(QIcon(":/deadlineDoneIcon"));
                }
                else if (selectedTask->timeRemaining() == 0)
                {
                    return QVariant(QIcon(":/deadlineDueIcon"));
                }
                else
                {
                    return QVariant(QIcon(":/deadlineTypeIcon"));
                }
            }

        }


        if (role == Qt::DisplayRole)
        {
            QString showText;

            if (selectedTask->type() == DEADLINE_TASK)
            {
                if (selectedTask->isDone())
                {
                    showText = QString("%1\nDone")
                            .arg(selectedTask->getTitle());
                }
                else
                {
                    DeadlineTask* dT = dynamic_cast<DeadlineTask*>(selectedTask);

                    showText = QString("%1\nDeadline: %2\n%3")
                            .arg(selectedTask->getTitle())
                            .arg(dT->getDeadline().toString())
                            .arg(dT->timeRemainingString());
                }
            }
            else if (selectedTask->type() == CYCLIC_TASK)
            {
                if (selectedTask->isDone())
                {
                    showText = QString("%1\nDONE for today!\n%2")
                            .arg(selectedTask->getTitle())
                            .arg(selectedTask->timeRemainingString());
                }
                else
                {
                    showText = QString("%1\n%2")
                            .arg(selectedTask->getTitle())
                            .arg(selectedTask->timeRemainingString());
                }
            }
            else
            {
                if (selectedTask->isDone())
                {
                    showText = QString("%1\nDone")
                            .arg(selectedTask->getTitle());
                }
                else
                {
                    showText = selectedTask->getTitle();
                }
            }

            //showText.append(QString(" (%1) ").arg(selectedTask->timeRemaining()));

            return QVariant(showText);
        }
        return QVariant();
    }
    else
    {
        return QVariant();
    }
}

AbstractTask *TaskListModel::taskForIndex(const QModelIndex &index) const
{
    int displayedAtRow = index.row();
    if (index.isValid() && taskOrder.contains(displayedAtRow))
    {
        return taskOrder.value(displayedAtRow);
    }
    else
    {
        return nullptr;
    }


}

void TaskListModel::setSortOrder(SortOrderType sortBy)
{
    if (sortOrder != sortBy)
    {
        sortOrder = sortBy;
        switch (sortBy) {
        case SORT_DEFAULT:
            qDebug() << "Sorting by default";
            break;
        case SORT_BY_URGENCY:
            qDebug() << "Sorting by urgency";
            break;
        case SORT_BY_DEADLINE:
            qDebug() << "Sorting by deadline";
            break;
        case SORT_BY_TIMELEFT:
            qDebug() << "Sorting by time left";
            break;

        default:
            qDebug() << "Sorting by DEFAULT SWITCH";
            break;
        }
        rebuildTaskOrder();
    }
}

//static comparator
bool TaskListModel::compareDeadline(const QDateTime &t1, const QDateTime &t2)
{

    if (t1.isNull() && !t2.isNull())
    {
        return false;
    }
    else if (!t1.isNull() && t2.isNull())
    {
        return true;
    }
    else
    {
        return t1 <= t2;
    }
}

//static comparator
bool TaskListModel::compareTaskPriority(const AbstractTask *&t1, const AbstractTask *&t2)
{
    return (t1->getImportance() > t2->getImportance());
}

//static comparator
bool TaskListModel::compareTimeRemaining(AbstractTask* &t1,   AbstractTask *&t2)
{
    qint64 t1Remain = t1->timeRemaining();
    qint64 t2Remain = t2->timeRemaining();

    if (t1Remain == t2Remain)
    {
        const AbstractTask* t_p1 = t1;
        const AbstractTask* t_p2 = t2;

        return compareTaskPriority(t_p1, t_p2);
    }

    if (t2Remain == -1)
        return true;
    else if (t1Remain == -1)
    {
        return false;
    }
    else
    {
        return t1Remain < t2Remain;
    }
}
