#include "CyclicTask.h"
#include <QDebug>

CyclicTask::CyclicTask(QObject* parent)
    : AbstractTask(parent)
{

}

const QDateTime CyclicTask::nextDue()
{
    QDateTime referencePoint;
    QDateTime dueTime;

    //set referencePoint from which to start calculation
    if (completion().isNull() || completion() < cycle.startTime)
    {   //never completed since startDateTime
        referencePoint = cycle.startTime;
    }
    else
    {
        referencePoint = completion();
        referencePoint.setTime(cycle.startTime.time());
        // Always assume original starting QTime is target time,
        // so completition-Times don't screw everything up.
    }


    switch(cycle.type)
    {
    case YEARLY:
        if (completion().isNull())
        {
            dueTime = referencePoint;
        }
        else
        {
            dueTime = referencePoint.addYears(cycle.periodLength);
        }
        break;
    case MONTHLY:
        if (completion().isNull())
        {
            dueTime = referencePoint;
        }
        else
        {
            dueTime = referencePoint.addMonths(cycle.periodLength);
        }
        break;
    case WEEKLY:
        if (completion().isNull())
        {
            dueTime = referencePoint;
        }
        else
        {
            dueTime = referencePoint.addDays(cycle.periodLength * 7);
        }
        break;

    case DAILY:
        if (completion().isNull())
        {
            dueTime = referencePoint;
        }
        else
        {
            dueTime = referencePoint.addDays(cycle.periodLength);
        }
        break;
    case WEEKLYDAYS:
    {
      //  qDebug() << "Calculating duetime for " << getTitle();
        int referenceWeekday;

        referenceWeekday = referencePoint.date().dayOfWeek();
        referenceWeekday--; //subtract 1 to normalise QDate::dayOfWeek() to Definitions::weekDayType
        //weekDayCompletition is now -1 for invalid completition day

        QList<WeekDayType> weekdayList = cycle.weekDays.toList();
        std::sort(weekdayList.begin(), weekdayList.end());

        //find smallest following day
        if (referenceWeekday >= weekdayList.last())
        {
        //    qDebug() << "no more possible days that week.";
            //completion date was on (or after) last weekday the task was scheduled for.
            //target date is the lowest date in cycleWeekDays of the following week.
            dueTime = referencePoint.addDays((7-referenceWeekday) + (int)(weekdayList[0]) );
        //    qDebug() << getTitle() << ": Next due date is @ " << dueTime.toString() << " NEXT week";
        }
        else
        {
         //   qDebug() << "more possibilities that week... looking into weekdays";
            //completition date is not the lowest possible weekday, so the dueDate is somewhen the same week.
            int nextScheduledDay = 0;
            //find smallest weekday following last completition date
            for (int i = 0; i < weekdayList.size(); ++i)
            {
            //    qDebug() << "comparing referenceWeekDay (" << referenceWeekday << ") with" << weekdayList.at(i);
                if (referenceWeekday <= weekdayList.at(i))
                {
                    nextScheduledDay = weekdayList.at(i);
            //        qDebug() << "yep, " << nextScheduledDay << "is the next possible weekDay";
                    break; //day found, break loop.
                }
                else
                {
                //    qDebug() << "nope, weekday " << weekdayList.at(i) << "had already passed";
                }
            }
            int daysToAdd = nextScheduledDay - referenceWeekday;
            dueTime = referencePoint.addDays(daysToAdd);
            /* qDebug() << QString("DaysToAdd = %1 (nextScheduledDay - referenceWeekday = %2-%3) ")
                        .arg(daysToAdd)
                        .arg(nextScheduledDay)
                        .arg(referenceWeekday); */
            // qDebug() << getTitle() << ": Next due date is @ " << dueTime.toString();
        }
    }
        break;

    default:
        dueTime = QDateTime();
        break;
    }

    return dueTime;
}

const bool CyclicTask::isDone()
{

    QDateTime currentTime(QDateTime::currentDateTime());

    if (completion().isNull())
    {
        return false;
    }

    QDateTime threshold = completion().addSecs(
                (nextDue().toSecsSinceEpoch() - completion().toSecsSinceEpoch())
                / 2);
    /* threshold is halfway between lastCompletion and nextDue,
     * and is the point in time from which on the task shall no longer
     * be considered completed */

    return currentTime < threshold;

}

const qint64 CyclicTask::timeRemaining()
{
    qint64 secondsRemaining =
            nextDue().toSecsSinceEpoch()
            - QDateTime::currentDateTime().toSecsSinceEpoch();
    if (secondsRemaining < 0 )
    {
        return 0;
    }
    else
    {
        return secondsRemaining;
    }
}


void CyclicTask::setCycleLength(int value)
{
    cycle.periodLength = value;
    emit taskChanged(this);
}


void CyclicTask::setCycle(const Cycle &value)
{
    cycle = value;
    emit taskChanged(this);
}

void CyclicTask::setCycleWeekdays(const QSet<WeekDayType> &value)
{
    cycle.weekDays = value;
    emit taskChanged(this);
}
