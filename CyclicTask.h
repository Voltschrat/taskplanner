#ifndef CYCLICTASK_H
#define CYCLICTASK_H

#include "AbstractTask.h"

#include <QObject>

class CyclicTask : public AbstractTask
{
public:
    CyclicTask(QObject *parent) ;

    // AbstractTask interface
public:
    const TaskType type() {return CYCLIC_TASK;};
    QDateTime const nextDue();
    bool const isDone();
    const qint64 timeRemaining();

    int cycleLength() const {return cycle.periodLength; };
    void setCycleLength(int value);

    Cycle getCycle() const {return cycle;};
    CycleType getCycleType() const {return cycle.type;};
    void setCycle(const Cycle &value);

    void setCycleWeekdays(const QSet<WeekDayType> &value);
    QSet<WeekDayType> weekdays() const {return cycle.weekDays;};

private :
    Cycle cycle;
   // QSet<WeekDayType> cycleWeekdays;

};

#endif // CYCLICTASK_H
